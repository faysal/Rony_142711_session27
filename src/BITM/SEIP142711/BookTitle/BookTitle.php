<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB
{
    public $id="";
    public $book_title="";
    public $writter_name="";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($postVariableData=NULL)
    {
        if (array_key_exists("id",$postVariableData) )
        {
            $this->id = $postVariableData['id'];
        }



        if (array_key_exists("book_title",$postVariableData) )
        {
            $this->book_title = $postVariableData['book_title'];
        }



        if (array_key_exists("writter_name",$postVariableData) )
        {
            $this->writter_name = $postVariableData['writter_name'];
        }
    } // end of set data



    public function store()
    {
        $arryData=array($this->book_title,$this->writter_name);//secure way...!!
        $sql="insert into book_title(book_title,writter_name)VALUES (?, ?)";
        $STH= $this->DBH->prepare($sql);
        $result = $STH->execute($arryData);

        if($result)
            Message::Message("Thank You Data Updated..!");
        else
            Message::Message("Sorry Your Data Save Unsucessful");
        Utility::redirect('create.php');
    }//sql statement store



    public function index($fetchMode='ASSOC')
    {
        $STH = $this->DBH->query('SELECT * from book_title');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;
    }// end of index();
    public function view($fetchMode='ASSOC')
    {

        $sql = 'SELECT * from book_title where id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();

    public function update()
    {
        $sql = "UPDATE book_title SET book_title =?, writter_name=? WHERE id=" .$this->id;

    } //end of update


}// end of book title


