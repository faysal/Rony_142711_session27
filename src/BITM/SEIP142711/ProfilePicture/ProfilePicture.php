<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class ProfilePicture extends DB{
    public $id;
    public $user_name;
    public $profile_picture;

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariableData=NULL)
    {
        if(array_key_exists("id",$postVariableData))
        {
            $this->id = $postVariableData['id'];
        }
        if(array_key_exists("name",$postVariableData))
        {
            $this->user_name = $postVariableData['name'];
        }
        if(array_key_exists("image_name",$postVariableData))
        {
            $this->profile_picture = $postVariableData['image_name'];
        }

       // var_dump($this);die();
    }

    public function store(){
        /*$sql = "insert into profile_picture(name,profile_picture)
                VALUES (' $this->name','$this->image_name')";

        echo $sql;

        die();*/

        $arrData = array($this->user_name,$this->profile_picture);

        $sql = "insert into profile_picture(user_name,profile_picture) VALUES ('$this->user_name', '$this->profile_picture')";

        $STH = $this->DBH->prepare($sql);  //prepare() object return kore
        $result = $STH->execute($arrData);
        //Utility::dd($result);      // arporer codegula run hbe na
        if($result)
            Message::Message("Thank You Data Updated..!");
        else
            Message::Message("Sorry Your Data Save Unsucessful");
        Utility::redirect('create.php');
    }

    public function index(){
        echo "I am inside the index method of Profile_Picture class<br>";
    }
}
